# Neighborhood Map (React)

This projects add interactivity to the app by refactoring the static code in the myreads template.

Of course, you are free to start this project from scratch if you wish! Just be sure to use [Create React App](https://github.com/facebookincubator/create-react-app) to bootstrap the project.

## TL;DR

To get started developing right away:

* install all project dependencies with `npm install`
* start the development server with `npm start`


Attempts to minimize use of components. Uses import/require statements to include them where they are needed.

* ListItem.js
* SearchBox.js (disabled)



## Third-Party API
* Google Maps API

## Additional Location Data
* Foursquare's API


## Asynchronicity and Error Handling
All data API's used in the application should load asynchronously, and errors should be handled gracefully.

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). You can find more information on how to perform common tasks [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).


## A Big Thank You
* Project Coaches: Y. Elhaony, Ryan Waite & Edoh Kodjo.
