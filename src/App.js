import React, { Component } from "react";
import "./App.css";
import axios from "axios";
import Map from "./components/Map";
import Header from "./components/Header";
import Search from "./components/Search";

class App extends Component {
  state = {
    allVenues: [],
    venues: [],
    markers: []
  };

  componentDidMount() {
    this.getPlaces();
  }

  renderMap = () => {
    loadScript(
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyC1CiDMv1byZZsxZ71mstyRoyYOtoU7nxQ&callback=initMap"
    );
    window.initMap = this.initMap;
  };

  getPlaces = () => {
    const endpoint = "https://api.foursquare.com/v2/venues/explore?";
    const param = {
      client_id: "QEDGLBW2DKG1VNT15ERQBMZCCLVKO4J3JDNP4UJQTLH35FBZ",
      client_secret: "SDEEK4HQN34GE24JP2TELDEJWX3CRI3V1X0YLO4CV15FF4CJ",
      query: "grocery",
      near: "96749",
      v: "20190212",
      limit: 8
    };

    axios
      .get(endpoint + new URLSearchParams(param))
      .then(res => {
        this.setState(
          {
            allVenues: res.data.response.groups[0].items,
            venues: res.data.response.groups[0].items
          },
          this.renderMap()
        );
      })
      .catch(error => {
        console.log(`Error: ${error}`);
      });
  };

  initMap = () => {
    let styledMapType = new window.google.maps.StyledMapType(
            [
              {elementType: 'geometry', stylers: [{color: '#ebe3cd'}]},
              {elementType: 'labels.text.fill', stylers: [{color: '#523735'}]},
              {elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}]},
              {
                featureType: 'administrative',
                elementType: 'geometry.stroke',
                stylers: [{color: '#c9b2a6'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'geometry.stroke',
                stylers: [{color: '#dcd2be'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ae9e90'}]
              },
              {
                featureType: 'landscape.natural',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#93817c'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry.fill',
                stylers: [{color: '#a5b076'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#447530'}]
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#f5f1e6'}]
              },
              {
                featureType: 'road.arterial',
                elementType: 'geometry',
                stylers: [{color: '#fdfcf8'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#f8c967'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#e9bc62'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry',
                stylers: [{color: '#e98d58'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry.stroke',
                stylers: [{color: '#db8555'}]
              },
              {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [{color: '#806b63'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.fill',
                stylers: [{color: '#8f7d77'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#ebe3cd'}]
              },
              {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [{color: '#b9d3c2'}]
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#92998d'}]
              }
            ],
            {name: 'Minimal'});

    let latlng = new window.google.maps.LatLng(19.547621, -155.013641);


    let mapOptions = {
      center: latlng,
      zoom: 11,
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
      }
    };

    let map = new window.google.maps.Map(document.getElementById("map"), mapOptions);


    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');



    // create InfoWindow
    let infowindow = new window.google.maps.InfoWindow({});

    // displays dynamic markers
    this.state.venues.forEach(x => {
      let contentString = `<div className="info"><h4>${x.venue.name}</h4>
        <p>${x.venue.location.formattedAddress}</p>
        <p>${x.venue.categories[0].name}</p>
        </div>`;

      // create marker
      let marker = new window.google.maps.Marker({
        position: {
          lat: x.venue.location.lat,
          lng: x.venue.location.lng
        },
        map: map,
        title: x.venue.name,
        id: x.venue.id,
        animation: window.google.maps.Animation.DROP
      });

      // adds marker to the markers arr
      this.state.markers.push(marker);

      // click and set content and open infowindow
      marker.addListener("click", () => {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);

        // marker animated
        marker.getAnimation() !== null
          ? marker.setAnimation(null)
          : marker.setAnimation(window.google.maps.Animation.BOUNCE);
      });
    });
  };

  updateVenues = newVenues => {
    this.setState({
      venues: newVenues
    });
  };

  render() {
    return (
      <div className="App">
        <Header />
        <main>
          <Search
            updateVenues={this.updateVenues}
            venues={this.state.allVenues}
            markers={this.state.markers}
          />
          <Map />
        </main>
      </div>
    );
  }
}

const loadScript = url => {
  const scriptIndex = window.document.getElementsByTagName("script")[0];
  let scriptTag = window.document.createElement("script");
  scriptTag.async = true;
  scriptTag.defer = true;
  scriptTag.src = url;
  scriptIndex.parentNode.insertBefore(scriptTag, scriptIndex);
};

export default App;
